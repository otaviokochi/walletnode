import * as express from "express";
import Payment from '../controller/payment.controller';
import { authenticate } from '../config/passport.config';

export const paymentRoute = (app: express.Application) => {
  const payment = new Payment();

  app.route('/payments')
    .all(authenticate())
    .get(payment.get)
    .post(payment.create)
  // .patch(payment.update)

  app.route('/payments/:id/estorno')
  .all(authenticate())
  .put(payment.chargeBack)

  app.route('/payments/:id')
    .all(authenticate())
    .post(payment.pay)
    // .put(payment.chargeBack)
}