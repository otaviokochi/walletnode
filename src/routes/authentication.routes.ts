import * as express from "express";
import signin from '../controller/authentication.controller';

export const authenticationRoute = (app: express.Application) => {
  app.post('/signin', signin)
}