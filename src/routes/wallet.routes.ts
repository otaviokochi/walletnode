import * as express from "express";
import WalletController from '../controller/wallet.controller';
import { authenticate } from '../config/passport.config';

export const walletRoute = (app: express.Application) => {
  const wallet = new WalletController();

  app.route('/wallets/money')
    .all(authenticate())
    .get(wallet.getMoney)

  app.route('/wallets')
    .post(wallet.create)

}