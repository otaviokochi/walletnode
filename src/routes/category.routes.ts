import * as express from "express";
import Category from '../controller/category.controller';
import { authenticate } from '../config/passport.config';

export const categoriesRoutes = (app: express.Application) => {
  const category = new Category();

  app.route('/categories')
    .all(authenticate())
    .get(category.get)
    .post(category.create)
    .patch(category.update)
}