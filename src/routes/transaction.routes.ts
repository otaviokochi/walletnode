import * as express from "express";
import Transaction from '../controller/transaction.controller';
import { authenticate } from '../config/passport.config';

export const transactionsRoute = (app: express.Application) => {
  const transaction = new Transaction();

  app.route('/transactions/csv')
    .all(authenticate())
    .post(transaction.downloadCSV)
    
  app.route('/transactions')
    .all(authenticate())
    .post(transaction.create)
    .get(transaction.get)

}