export { };

import express from 'express';
import cors from 'cors';
import passport from 'passport';

const app = express();
import dotenv from "dotenv";

dotenv.config();

app.use(cors())
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//routes
import { authenticationRoute } from './routes/authentication.routes';
authenticationRoute(app);
import { categoriesRoutes } from './routes/category.routes';
categoriesRoutes(app);
import { transactionsRoute } from './routes/transaction.routes';
transactionsRoute(app);
import { walletRoute } from './routes/wallet.routes';
walletRoute(app);
import { paymentRoute } from './routes/payment.routes';
paymentRoute(app);

app.use(passport.initialize());

export default app;