export { };

import knex from '../../database/db';

class Wallet {
  cpf: string;
  name: string;
  money?: number;
  password: string;

  constructor(user: Wallet) {
    this.cpf = user.cpf;
    this.name = user.name;
    this.money = 0;
    this.password = user.password;
  }

  static async create(newUser: Wallet) {
    return knex('wallets').insert({
      cpf: newUser.cpf,
      name: newUser.name,
      money: 0,
      password: newUser.password
    }, 'id');
  }

  static async getByCpf(cpf: string) {
    return knex('wallets')
      .where('cpf', cpf)
      .first()
      .select("id", "name", "money", "cpf");
  }

  static async getById(idWallet: number) {
    return knex('wallets')
      .where('id', idWallet)
      .first()
      .select("id", "name", "money", "cpf");
  }

  static async getMoneyById(idWallet: number) {
    return knex('wallets')
      .where('id', idWallet)
      .select('money')
      .first();
  }

  static async updateMoney(idWallet: number, newMoney: number) {
    return knex('wallets')
      .where('id', idWallet)
      .update({
        money: newMoney
      });
  }

  static async remove(id: number) { return knex('wallets').where('id', id).del(); }
}

export default Wallet;