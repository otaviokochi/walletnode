export { };
import knex from '../../database/db';

class Payment {
  id?: number;
  dueDate: Date;
  periodicity: number;
  timesWillBeRepeated: number;
  idUserWhoSent: number;
  nameUserWhoSent: string;
  idUserWhoReceived: number;
  nameUserWhoReceived: string;
  automaticPayment: boolean;
  paymentMoney: number;
  created_at?: any;
  timesExecuted: number;
  isActive?: boolean;

  constructor(payment: Payment) {
    this.id = payment.id,
    this.dueDate = payment.dueDate,
    this.periodicity = payment.periodicity,
    this.timesWillBeRepeated = payment.timesWillBeRepeated,
    this.idUserWhoSent = payment.idUserWhoSent,
    this.nameUserWhoSent = payment.nameUserWhoSent,
    this.idUserWhoReceived = payment.idUserWhoReceived,
    this.nameUserWhoReceived = payment.nameUserWhoReceived,
    this.automaticPayment = payment.automaticPayment,
    this.paymentMoney = payment.paymentMoney,
    this.timesExecuted = payment.timesExecuted,
    this.isActive = true
  }

  static async create(newPayment: Payment) {
    return knex('payments').insert({
      dueDate: newPayment.dueDate,
      periodicity: newPayment.periodicity,
      timesWillBeRepeated: newPayment.timesWillBeRepeated,
      idUserWhoSent: newPayment.idUserWhoSent,
      nameUserWhoSent: newPayment.nameUserWhoSent,
      idUserWhoReceived: newPayment.idUserWhoReceived,
      nameUserWhoReceived: newPayment.nameUserWhoReceived,
      automaticPayment: newPayment.automaticPayment,
      paymentMoney: newPayment.paymentMoney,
      timesExecuted: newPayment.timesExecuted,
      isActive: newPayment.isActive,
    }, "id");
  }

  static async getAll() { return knex('payments'); }

  static async getAllUserPayments(idUser: number) {
    return knex('payments')
      .where('idUserWhoSent', idUser)
      .orWhere('idUserWhoReceived', idUser)
  }

  static async getAllUserTransactionByCategory(idUser: number, category: number) {
    return knex('payments')
      .where({
        idUser,
        category
      })
  }
  
  static async update(id: number, payment: Payment) {
    return knex('payments')
      .where('id', id)
      .update({
        ...payment
      })
  }

  static async remove(id: number) { return knex('payments').where('id', id).del(); }
}

export default Payment;