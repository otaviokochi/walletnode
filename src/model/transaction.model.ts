export { };
import knex from '../../database/db';

class Transaction {
  idUserWhoSent: number;
  idUserWhoReceived: number;
  transferredMoney: number;
  nameUserWhoSent: string;
  nameUserWhoReceived: string;
  obs: string;
  category: string;
  created_at?: any;

  constructor(transaction: Transaction) {
    this.idUserWhoSent = transaction.idUserWhoSent,
    this.idUserWhoReceived = transaction.idUserWhoReceived,
    this.nameUserWhoSent = transaction.nameUserWhoSent,
    this.nameUserWhoReceived = transaction.nameUserWhoReceived,
    this.transferredMoney = transaction.transferredMoney,
    this.obs = transaction.obs,
    this.category = transaction.category
  }

  static async create(newTransaction: Transaction) {
    return knex('transctions').insert({
      idUserWhoSent: newTransaction.idUserWhoSent,
      transferredMoney: newTransaction.transferredMoney,
      idUserWhoReceived: newTransaction.idUserWhoReceived,
      obs: newTransaction.obs,
      category: newTransaction.category
    }, "id");
  }

  static async getAll() { return knex('transctions'); }

  static async getAllUserTransaction(idUser: number) {
    return knex('transactions')
      .where('idUserWhoSent', idUser)
      .orWhere('idUserWhoReceived', idUser)
  }

  static async getAllUserTransactionByCategory(idUser: number, category: number) {
    return knex('transactions')
      .where({
        idUser,
        category
      })
  }

  static async remove(id: number) { return knex('transactions').where('id', id).del(); }
}

export default Transaction;