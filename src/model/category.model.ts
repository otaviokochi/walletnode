export { };

import knex from '../../database/db';

class Category {
  name: string;
  idUser: number;

  constructor(category: Category) {
    this.name = category.name;
    this.idUser = category.idUser;
  }

  static async get(idUser: number) {
    return knex('category')
      .where('idUser', idUser)
  }

  static async create(id: number, newCategory: string) {
    return knex('category')
      .insert({
        idUser: id,
        name: newCategory
      })
  }

  static async update(id: number, idCategory: number, updatedCategory: string) {
    return knex('category')
      .where('id', idCategory)
      .andWhere('idUser', id)
      .update({
        name: updatedCategory
      });
  }

  static async remove(id: number, idUser: number) { 
    return knex('category')
    .where('id', id)
    .andWhere('idUser', idUser)
    .del(); 
}
}

export default Category;