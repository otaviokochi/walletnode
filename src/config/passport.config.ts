export { };

import passport from 'passport';
import passportJwt from 'passport-jwt';
const { Strategy, ExtractJwt } = passportJwt;
import knex from '../../database/db';

const params = {
  secretOrKey: process.env.AUTH_SECRET,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}

const strategy = new Strategy(params, (payload: {
  id: number,
  iat: number,
  name: string,
  cpf: string,
  exp: number
}, done: any) => {
  knex('wallets')
    .where('id', payload.id)
    .first()
    .then((user: any) => {
      if (!user) return done(null, false);
      if (user.cpf === payload.cpf) return done(null, { ...payload });
      else return done(null, false);
    })
    .catch((err: any) => done(err, false))
})

passport.use(strategy);

export const authenticate = () => passport.authenticate('jwt', { session: false })