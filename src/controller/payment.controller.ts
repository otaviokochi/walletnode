export { };
import { Request, Response } from 'express';

import Transaction from '../model/transaction.model';
import Payment from '../model/payment.model';
import Wallet from '../model/wallet.model';
import knex from '../../database/db';
import jwt from 'jwt-simple';
import dateFormat from '../helper/dateFormat'

class PaymentController {
  async create(req: Request, res: Response) {
    const { cpf, paymentMoney } = req.body;
    const moneyFloat = parseFloat(paymentMoney);
    if (isNaN(moneyFloat) || moneyFloat === 0) return res.status(400).send({ message: "Valor inválido!" });
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;
    console.log(req.body)
    const userReceiving = await Wallet.getByCpf(cpf)
      .then((response: any) => {
        console.log(response)
        if (response)
          return response;
        return new Error('Cliente não encontrado!');
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (userReceiving instanceof Error && userReceiving.message == "Cliente não encontrado!")
      return res.status(400).send(userReceiving.message);
    if (userReceiving instanceof Error) return res.status(500).send({ message: "Erro no servidor" });
    if (id == userReceiving.id) return res.status(400).send({ message: "Impossível realizar o pagamento!" });

    const userSending = await Wallet.getById(id)
      .then((response: any) => {
        if (response)
          return response;
        return new Error('Cliente não encontrado!');
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (userSending.money instanceof Error) return res.status(500).send({ message: "Error" });

    const payment = new Payment({
      dueDate: req.body.dueDate,
      periodicity: req.body.periodicity,
      timesWillBeRepeated: req.body.timesWillBeRepeated,
      idUserWhoSent: id,
      nameUserWhoSent: userSending.name,
      idUserWhoReceived: userReceiving.id,
      nameUserWhoReceived: userReceiving.name,
      automaticPayment: req.body.automaticPayment,
      paymentMoney: req.body.paymentMoney,
      timesExecuted: 0,
    })

    const responseCreate = await Payment.create(payment)
      .then((response: any) => {
        console.log(response);
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (responseCreate instanceof Error) return res.status(500).send({ message: "Erro ao criar novo pagamento!" });
    res.status(200).send({ message: "Pagamento recorrente criado com sucesso!" });
  };

  async get(req: Request, res: Response) {
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;

    const payments = await Payment.getAllUserPayments(id)
      .then((response: any) => {
        return response;
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })
    if (payments instanceof Error) return res.status(500).send({ message: "Erro ao recuperar as transações, tente novamente!" });
    res.status(200).send(payments);
  };


  async pay(req: Request, res: Response) {
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;

    const allPayments = await Payment.getAllUserPayments(id)
      .then((response: any) => {
        return response;
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })
    if (allPayments instanceof Error) return res.status(500).send({ message: "Erro ao recuperar as transações, tente novamente!" });
    const payment = allPayments.find((element: Payment) => element.id == parseInt(req.params.id))
    payment.timesExecuted++;

    //checking if payment already executed payment.timesWillBeRepeated times.
    payment.isActive = payment.timesWillBeRepeated <= payment.timesExecuted - 1 ? false : true;
    let day: number, year: number, month: number;
    let stringDueDate: string;
    switch (payment.periodicity) {
      case 1:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        if (day > 30) {
          day = 1;
          if (month >= 12) {
            month = 1;
            year++;
          }
          else month++;
        } else day++;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate
        break;
      case 2:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        if (day + 7 > 30) {
          day = day - 30 + 7;
          if (month >= 12) {
            month = 1;
            year++;
          }
          else month++;
        } else day += 7;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate;
        break;
      case 3:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        if (day + 14 > 30) {
          day = day - 30 + 14;
          if (month >= 12) {
            month = 1;
            year++;
          }
          else month++;
        } else day += 14;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate;
        break;
      case 4:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        if (month + 6 > 12) {
          month = 1;
          year++;
        }
        else month += 6;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate;
        break;
      case 5:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        year++;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate;
        break;
    }

    const sentTransaction = new Transaction({
      idUserWhoSent: payment.idUserWhoSent,
      nameUserWhoSent: payment.nameUserWhoSent,
      nameUserWhoReceived: payment.nameUserWhoReceived,
      idUserWhoReceived: payment.idUserWhoReceived,
      transferredMoney: -payment.paymentMoney,
      obs: null,
      category: null
    });

    const userReceiving = await Wallet.getById(payment.idUserWhoReceived)
      .then((response: any) => {
        console.log(response)
        if (response)
          return response;
        return new Error('Cliente não encontrado!');
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })
    if (userReceiving instanceof Error && userReceiving.message == "Cliente não encontrado!")
      return res.status(400).send(userReceiving.message);
    if (userReceiving instanceof Error) return res.status(500).send({ message: "Erro no servidor" });
    if (id == userReceiving.id) return res.status(400).send({ message: "Impossível realizar a transação!" });

    const userSending = await Wallet.getById(id)
      .then((response: any) => {
        if (response)
          return response;
        return new Error('Cliente não encontrado!');
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (userSending instanceof Error) return res.status(500).send({ message: "Error" });
    if (userSending.money - payment.paymentMoney < 0) return res.status(400)
      .send({ message: "Usuário não possui dinheiro suficiente para transação" });

    knex.transaction((trx: any) => {
      return trx
        .insert([sentTransaction], 'id')
        .into('transactions')
        .then(async _ => {
          return await Promise.all([
            trx('wallets').where('id', id).update({
              money: userSending.money - payment.paymentMoney
            }),
            trx('wallets').where('id', userReceiving.id).update({
              money: userReceiving.money + payment.paymentMoney
            }),
            trx('payments').where('id', payment.id).update({
              ...payment
            })
          ]);
        })
        .catch((error: any) => {
          throw Error(error)
        })
    })
      .then((response: any) => {
        console.log(response);
        res.json({ message: `Pagamento no valor de R$${payment.paymentMoney} para o cpf ${userReceiving.cpf} realizada com sucesso!` });
      })
      .catch((error: any) => {
        //None of those datas has been saved in database
        console.error(error);
        res.status(500).send({ message: `Erro no pagamento` });
      });

    // res.status(200).send(payments);
  };

  async chargeBack(req: Request, res: Response) {
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;

    const allPayments = await Payment.getAllUserPayments(id)
      .then((response: any) => {
        return response;
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })
    if (allPayments instanceof Error) return res.status(500).send({ message: "Erro ao recuperar as transações, tente novamente!" });
    const payment = allPayments.find((element: Payment) => element.id == parseInt(req.params.id))
    if (payment.timesExecuted <= 0) return res.status(400).send({ message: "Impossível realizar estorno!" });
    payment.timesExecuted--;

    //checking if payment already executed payment.timesWillBeRepeated times.
    payment.isActive = payment.timesWillBeRepeated <= payment.timesExecuted - 1 ? false : true;
    let day: number, year: number, month: number;
    let stringDueDate: string;
    switch (payment.periodicity) {
      case 1:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        if (day == 1) {
          day = 30;
          if (month == 1) {
            month = 12;
            year--;
          }
          else month--;
        } else day--;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate
        break;
      case 2:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        if (day - 7 <= 0) {
          day = day + 30 - 7;
          if (month == 1) {
            month = 12;
            year--;
          }
          else month--;
        } else day -= 7;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate;
        break;
      case 3:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        if (day - 14 <= 0) {
          day = day + 30 - 14;
          if (month == 1) {
            month = 12;
            year--;
          }
          else month--;
        } else day -= 14;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate;
        break;
      case 4:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        if (month - 6 < 1) {
          month = month + 12 - 6;
          year--;
        }
        else month -= 6;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate;
        break;
      case 5:
        day = parseInt(payment.dueDate.split("-")[2]);
        month = parseInt(payment.dueDate.split("-")[1]);
        year = parseInt(payment.dueDate.split("-")[0]);
        year--;
        stringDueDate = dateFormat(year, month, day);
        payment.dueDate = stringDueDate;
        break;
    }

    const sentTransaction = new Transaction({
      idUserWhoSent: payment.idUserWhoReceived,
      nameUserWhoSent: payment.nameUserWhoReceived,
      nameUserWhoReceived: payment.nameUserWhoSent,
      idUserWhoReceived: payment.idUserWhoSent,
      transferredMoney: -payment.paymentMoney,
      obs: "estorno",
      category: null
    });

    const userReceiving = await Wallet.getById(id)
      .then((response: any) => {
        console.log(response)
        if (response)
          return response;
        return new Error('Cliente não encontrado!');
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (userReceiving instanceof Error && userReceiving.message == "Cliente não encontrado!")
      return res.status(400).send(userReceiving.message);
    if (userReceiving instanceof Error) return res.status(500).send({ message: "Erro no servidor" });
    console.log("AAAAAA")

    const userSending = await Wallet.getById(payment.idUserWhoReceived)
      .then((response: any) => {
        if (response)
          return response;
        return new Error('Cliente não encontrado!');
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (userSending instanceof Error) return res.status(500).send({ message: "Error" });
    if (userSending.money - payment.paymentMoney < 0) return res.status(400)
      .send({ message: "Usuário não possui dinheiro suficiente para transação" });

    knex.transaction((trx: any) => {
      return trx
        .insert([sentTransaction], 'id')
        .into('transactions')
        .then(async _ => {
          return await Promise.all([
            trx('wallets').where('id', userSending.id).update({
              money: userSending.money - payment.paymentMoney
            }),
            trx('wallets').where('id', userReceiving.id).update({
              money: userReceiving.money + payment.paymentMoney
            }),
            trx('payments').where('id', payment.id).update({
              ...payment
            })
          ]);
        })
        .catch((error: any) => {
          throw Error(error)
        })
    })
      .then((response: any) => {
        console.log(response);
        res.json({ message: `Estorno no valor de R$${payment.paymentMoney} realizada com sucesso!` });
      })
      .catch((error: any) => {
        //None of those datas has been saved in database
        console.error(error);
        res.status(500).send({ message: `Erro no pagamento` });
      });
    // res.status(200).send(payments);
  };
}

export default PaymentController;