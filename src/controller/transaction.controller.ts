export { };
import { Request, Response } from 'express';

import Transaction from '../model/transaction.model';
import Wallet from '../model/wallet.model';
import knex from '../../database/db';
import jwt from 'jwt-simple';
import dateFormat from '../helper/dateFormat'

class TransactionsController {

  async create(req: Request, res: Response) {
    const { cpf, money, obs, category } = req.body;
    const moneyFloat = parseFloat(money);
    if(isNaN(moneyFloat) || moneyFloat === 0) return res.status(400).send({ message: "Valor inválido!" });
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;

    const userReceiving = await Wallet.getByCpf(cpf)
      .then((response: any) => {
        console.log(response)
        if (response)
          return response;
        return new Error('Cliente não encontrado!');
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })
    if (userReceiving instanceof Error && userReceiving.message == "Cliente não encontrado!")
      return res.status(400).send(userReceiving.message);
    if (userReceiving instanceof Error) return res.status(500).send({ message: "Erro no servidor" });
    if (id == userReceiving.id) return res.status(400).send({ message: "Impossível realizar a transação!" });

    const userSending = await Wallet.getById(id)
      .then((response: any) => {
        if (response)
          return response;
        return new Error('Cliente não encontrado!');
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (userSending instanceof Error) return res.status(500).send({ message: "Error" });
    if (userSending.money - moneyFloat < 0) return res.status(400)
      .send({ message: "Usuário não possui dinheiro suficiente para transação" });

    const sentTransaction = new Transaction({
      idUserWhoSent: id,
      nameUserWhoSent: userSending.name,
      nameUserWhoReceived: userReceiving.name,
      idUserWhoReceived: userReceiving.id,
      transferredMoney: -moneyFloat,
      obs: obs,
      category: category
    });

    //rollback if any of the inserts/updtates fails.
    knex.transaction((trx: any) => {
      return trx
        .insert([sentTransaction], 'id')
        .into('transactions')
        .then(async _ => {
          return await Promise.all([
            trx('wallets').where('id', id).update({
              money: userSending.money - moneyFloat
            }),
            trx('wallets').where('id', userReceiving.id).update({
              money: userReceiving.money + moneyFloat
            })
          ]);
        })
        .catch((error: any) => {
          throw Error(error)
        })
    })
      .then((response: any) => {
        console.log(response);
        res.json({ message: `Transferência no valor de R$${moneyFloat} para o cpf ${userReceiving.cpf} realizada com sucesso!` });
      })
      .catch((error: any) => {
        //None of those datas has been saved in database
        console.error(error);
        res.status(500).send({ message: `Erro na transferência` });
      });

  };

  async get(req: Request, res: Response) {
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;

    const transactions = await Transaction.getAllUserTransaction(id)
      .then((response: any) => {
        if (req.body.initialDate && req.body.finalDate) {
          //creating a new Date object from query parameters.
          const finalDate = new Date(req.query.finalDate.toString());
          const initialDate = new Date(req.query.initialDate.toString());
          //getting date in string format.
          const stringInitialDate = dateFormat(initialDate.getFullYear(), initialDate.getMonth(), initialDate.getDate());
          const stringFinalDate = dateFormat(finalDate.getFullYear(), finalDate.getMonth(), finalDate.getDate());
          return response.filter((transaction: Transaction) => {
            const stringTransactionDate =
              dateFormat(transaction.created_at.getFullYear(), transaction.created_at.getMonth(), transaction.created_at.getDate());
            return stringTransactionDate >= stringInitialDate && stringTransactionDate <= stringFinalDate;
          })
        } else return response;
      })
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (transactions instanceof Error) return res.status(500).send({ message: "Erro ao recuperar as transações, tente novamente!" });

    res.status(200).send(transactions);
  };

  async downloadCSV(req: Request, res: Response) {
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    
    req.body.transactions.forEach((transaction: Transaction) => {
      delete transaction.idUserWhoReceived;
      delete transaction.idUserWhoSent;
      delete transaction.nameUserWhoReceived;
      delete transaction.nameUserWhoSent;
    })

    const csvWriter = createCsvWriter({
      path: 'transcations.csv',
      header: [
        { id: 'category', title: 'Nome categoria' },
        { id: 'created_at', title: 'Dia da transação realizada' },
        { id: 'name', title: 'Destinatário/Remetente' },
        { id: 'obs', title: 'Observação' },
        { id: 'transferredMoney', title: 'Quantidade dinheiro' },
        { id: 'type', title: 'Tipo de transação' },
      ]
    });
    csvWriter
      .writeRecords(req.body.transactions)
      .then(() => res.status(200).send({ message: 'Arquivo CSV baixado com sucesso' }))
      .catch(() => res.status(500).send({ message: "Erro ao criar arquivo CSV" }));
  };

}

export default TransactionsController;