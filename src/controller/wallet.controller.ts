export { };
import { Request, Response } from 'express';

import Wallet from '../model/wallet.model';
import jwt from 'jwt-simple';
import bcrypt from 'bcrypt';
const UNIQUEVIOLATIONCODE = 23505;

class WalletController {
  async create(req: Request, res: Response) {
    if (!req.body.password || !req.body.cpf || !req.body.name) return res.status(400).send({ message: 'Dados para criação faltando!' });

    const password = await bcrypt.hash(req.body.password, 10).catch((error: any) => '');
    console.log(password);
    if (password) {
      const wallet = new Wallet({
        name: req.body.name,
        cpf: req.body.cpf,
        password
      });

      const response = await Wallet.create(wallet)
        .catch((error: any) => {
          console.log(error);
          if (error.code == UNIQUEVIOLATIONCODE) {
            res.status(400).send({ message: `Usuário de cpf ${wallet.cpf} já criado!` });
          } else {
            res.status(500);
          }
          return new Error(error);
        })
      if (response instanceof Error) return;
      console.log(response);
      res.status(200).json(`Carteira do usuário ${wallet.cpf} criada!`);
    } else {
      res.status(400).send({ message: 'Dados para criação faltando!' });
    }
  };

  async getMoney(req: Request, res: Response) {
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;

    const response = await Wallet.getMoneyById(id)
      .then()
      .catch((error : any) => {
        console.log(error);
        return new Error(error);
      })

    if (response instanceof Error) return res.status(500).send({ message: "Erro ao recuperar seu saldo, tente novamente!" });

    res.status(200).send(response);
  }

}


export default WalletController;