export { };
import { Request, Response } from 'express';
import Category from '../model/category.model';
import jwt from 'jwt-simple';

class CategoryController {

  async create(req: Request, res: Response) {
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;
    //getting all categories to insert another one
    const categories = await Category.get(id).catch(error => {
      console.log(error);
      return Error(error);
    });

    if (categories instanceof Error) return res.status(500).send({ message: "Erro ao criar categoria!" });
    const categoryAlreadyExists = categories.find((category: Category) => category.name == req.body.category)
    if (categoryAlreadyExists) return res.status(400).send({ message: "Categoria já existe!" });

    const response = await Category.create(id, req.body.category)
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })
    if (response instanceof Error && response.message == "Error: Categoria já existe!") return res.status(400).send({ message: "Categoria já existe!" });
    if (response instanceof Error) return res.status(500).send({ message: "Erro ao criar categoria!" });

    res.status(200).send({ message: `Categoria ${req.body.category} criada com sucesso!` })
  }

  async get(req: Request, res: Response) {

    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;

    const categories = await Category.get(id)
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })
    if (categories instanceof Error) return res.status(500).send({ message: "Erro ao buscar as categorias do usuário" });
    if (categories) {
      res.status(200).send(categories)
    } else {
      res.status(200).send({
        message: "Nenhuma categoria encontrada!"
      })
    }
  }

  async update(req: Request, res: Response) {
    const token = req.headers['authorization'].split(' ')[1];
    const id = jwt.decode(token, process.env.AUTH_SECRET).id;

    //getting all categories to update one that alredy exists
    const categories = await Category.get(id).catch(error => {
      console.log(error);
      return Error(error);
    });
    if (categories instanceof Error) return res.status(500).send({ message: "Erro ao atualizar categoria!" });
    const categoryAlreadyExists = categories.find((category: Category) => category.name == req.body.oldCategory);

    if (!categoryAlreadyExists) return res.status(400).send({ message: "Categoria não encotrada!" });

    const response = await Category.update(id, categoryAlreadyExists.id, req.body.updatedCategory)
      .catch((error: any) => {
        console.log(error);
        return new Error(error);
      })

    if (response instanceof Error) return res.status(500).send({ message: "Erro ao atualizar a categoria do usuário!" });

    res.status(200).send({ message: `Categoria ${req.body.oldCategory} atualizada com sucesso!` })
  }
}

export default CategoryController;