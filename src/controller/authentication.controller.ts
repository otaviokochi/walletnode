export { };
import { Request, Response } from 'express';

import jwt from 'jwt-simple';
import bcrypt from 'bcrypt';
import knex from '../../database/db';

const signin = async (req: Request, res: Response) => {
  const { cpf, password } = req.body;

  if (!cpf || !password) return res.status(401).send('Cpf/senha inválidos');

  const user = await knex('wallets').where('cpf', cpf).first().catch((err: any) => {
    console.log(err);
    return false;
  });
  if (!user) return res.status(401).send('Cpf/senha inválidos');

  const matchPassword = await bcrypt.compare(password, user.password).catch((err: any) => {
    console.log(err);
    return false;
  });

  if (!matchPassword) return res.status(401).send('Cpf/senha inválidos');

  const now = Math.floor(Date.now() / 1000);

  const payload = {
    id: user.id,
    iat: now,
    name: user.name,
    cpf: user.cpf,
    exp: now + (60 * 60 * 24 * 3)
  }

  res.json({
    ...payload,
    token: jwt.encode(payload, process.env.AUTH_SECRET)
  })
}

export default signin;