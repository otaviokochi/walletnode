export default function dateFormat(YYYY: number, MM: number, DD: number) {
  let month: string, day: string;
  if (MM < 10) 
    month = `0${MM}`
  else
    month = `${MM}`
  if (DD < 10)
    day = `0${DD}`
  else
    day = `${DD}`
  return `${YYYY}-${month}-${day}`;
}