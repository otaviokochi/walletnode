# Wallet

## Objetivos
  - Construir um sistema que gerencie uma carteira virtual.

## Instruções para rodar
  - Criar um arquivo ```.env``` na raíz do projeto com as seguintes variáveis e atribuir os seus valores:
  ```javascript
    //exemplo
    USER_DB=postgres
    PASSWORD=otavio
    DATABASE=wallet
    DATABASE_TEST=wallettest
    AUTH_SECRET=ausodfpela
  ```
  - Rodar o comando ```npm install```
  - Rodar o comando ```knex migrate:latest```
  > Caso não possua o knex instalado globalmente utilize ```npx knex migrate:latest```
  - Rodar o comando ```knex seed:run --specific=wallet.ts && npx knex seed:run --specific=categories.ts```
  > Caso não possua o knex instalado globalmente utilize ```npx knex seed:run --specific=wallet.ts && npx knex seed:run --specific=categories.ts```

  >Porta utilizada para backend: 5001
  - Rodar o comando npm start
  - Utilizar o repositório crudFront para o front-end
