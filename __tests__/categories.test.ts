process.env.NODE_ENV = 'test';

import request from 'supertest';
import app from '../src/app';

let token: string;

beforeAll(async () => {
  token = await request(app).post('/signin').send({
    cpf: "1111",
    password: "otavio"
  }).then(response => response.body.token)
})

describe("Test categories api", () => {
  it("Test create new category", async () => {
    const req = await request(app).post('/categories').set("Authorization", `bearer ${token}`).send({
      category: "nova categoria usuário 1",
    });
    expect(req.status).toBe(200);
    expect(req.body.message).toEqual(`Categoria nova categoria usuário 1 criada com sucesso!`);
  })

  it("Test create category that already exists", async () => {
    const req = await request(app).post('/categories').set("Authorization", `bearer ${token}`).send({
      category: "nova categoria usuário 1",
    });
    expect(req.status).toBe(400);
    expect(req.body.message).toEqual(`Categoria já existe!`);
  })

  it("Update category that exists", async () => {
    const req = await request(app).patch('/categories').set("Authorization", `bearer ${token}`).send({
      oldCategory: "nova categoria usuário 1",
      updatedCategory: "categoria atualizada",
    });
    expect(req.status).toBe(200);
    expect(req.body.message).toEqual('Categoria nova categoria usuário 1 atualizada com sucesso!');
  })

  it("Update category that doesn't exists", async () => {
    const req = await request(app).patch('/categories').set("Authorization", `bearer ${token}`).send({
      oldCategory: "nova categoria usuário 1",
      updatedCategory: "categoria atualizada",
    });
    expect(req.status).toBe(400);
    expect(req.body.message).toEqual("Categoria não encotrada!");
  })

})
