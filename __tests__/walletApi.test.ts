process.env.NODE_ENV = 'test';

import request from 'supertest';
import app from '../src/app';

let token: string;

beforeAll(async () => {
  token = await request(app).post('/signin').send({
    cpf: "1111",
    password: "otavio"
  }).then(response => response.body.token)
})

describe("Test wallet api", () => {
  it("Test create new wallet", async () => {
    const req = await request(app).post('/wallets').send({
      cpf: "8885",
      name: "newClient",
      password: "otavio"
    });
    expect(req.status).toBe(200);
    expect(req.body).toEqual(`Carteira do usuário 8885 criada!`);
  })

  it("Test create new wallet with missing params", async () => {
    const req = await request(app).post('/wallets').send({
      name: "newClient",
      password: "otavio"
    });
    expect(req.status).toBe(400);
    expect(req.body.message).toEqual('Dados para criação faltando!');
  })

  it("Test create new wallet with missing params", async () => {
    const req = await request(app).post('/wallets').send({
      cpf: "8885",
      name: "newClient",
    });
    expect(req.status).toBe(400);
    expect(req.body.message).toEqual('Dados para criação faltando!');
  })

  it("Test get wallet money", async () => {
    const req = await request(app).get('/wallets/money').set("Authorization", `bearer ${token}`);
    expect(req.status).toBe(200);
    expect(req.body.money).toEqual(1050.5);
  })

})
