process.env.NODE_ENV = 'test';

import request from 'supertest';
import app from '../src/app';

let token: string;

beforeAll(async () => {
  token = await request(app).post('/signin').send({
    cpf: "0101",
    password: "otavio"
  }).then(response => response.body.token)
})

describe("Test transaction api", () => {

  const transaction = [{
    id: 1,
    idUserWhoSent: 3,
    nameUserWhoSent: 'Usuário 2',
    transferredMoney: -500,
    idUserWhoReceived: 2,
    nameUserWhoReceived: "Usuário 1",
    obs: "envio",
    category: null,
    created_at: expect.any(String),
  }]
  
  it("Test create new transacion", async () => {
    const req = await request(app).post('/transactions').set("Authorization", `bearer ${token}`).send({
      cpf: "1010",
      money: 500,
      obs: "envio"
    });
    expect(req.status).toBe(200);
    expect(req.body.message).toEqual(`Transferência no valor de R$500 para o cpf 1010 realizada com sucesso!`);
  })

  it("Test create new transaction without money", async () => {
    const req = await request(app).post('/transactions').set("Authorization", `bearer ${token}`).send({
      cpf: "1010",
      obs: "envio"
    });
    expect(req.status).toBe(400);
    expect(req.body.message).toEqual("Valor inválido!");
  })

  it("Test create new transaction with more money than user has", async () => {
    const req = await request(app).post('/transactions').set("Authorization", `bearer ${token}`).send({
      cpf: "1010",
      money: 10000,
      obs: "envio"
    });
    expect(req.status).toBe(400);
    expect(req.body.message).toEqual("Usuário não possui dinheiro suficiente para transação");
  })

  it("Test create new transaction to the same user", async () => {
    const req = await request(app).post('/transactions').set("Authorization", `bearer ${token}`).send({
      cpf: "0101",
      money: 150,
      obs: "envio"
    });
    expect(req.status).toBe(400);
    expect(req.body.message).toEqual("Impossível realizar a transação!");
  })

  it("Test create new transaction to user that doesn't exists", async () => {
    const req = await request(app).post('/transactions').set("Authorization", `bearer ${token}`).send({
      cpf: "8888888",
      money: 500,
      obs: "envio"
    });
    expect(req.status).toBe(400);
    expect(req.text).toEqual("Cliente não encontrado!");
  })

  it("Test get user transcations", async () => {
    const req = await request(app).get('/transactions').set("Authorization", `bearer ${token}`);
    expect(req.status).toBe(200);
    expect(req.body).toEqual(transaction);
  })

})
