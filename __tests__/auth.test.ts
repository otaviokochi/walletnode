process.env.NODE_ENV = 'test';

import app from '../src/app';
import request from "supertest";


describe("Authenticate api test", () => {
  it('Authenticate with a user that exists in database', async () => {
    const response = await request(app).post('/signin').send({
      cpf: '1111',
      password: 'otavio'
    })

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      id: 1,
      name: 'otavio',
      iat: expect.any(Number),
      cpf: '1111',
      exp: expect.any(Number),
      token: expect.any(String)
    });
  })

  it('Authenticate with a user that does not exists in database', async () => {
    const response = await request(app).post('/signin').send({
      cpf: "99999",
      password: "otaviokochi"
    });
    expect(response.status).toBe(401);
    expect(response.text).toBe('Cpf/senha inválidos');
  })

  it('Authenticate with wrong parameter', async () => {
    const response = await request(app).post('/signin').send({
      user: "1111",
      password: "otaviokochi"
    });
    expect(response.status).toBe(401);
    expect(response.text).toBe('Cpf/senha inválidos');
  })
})
