import knex from './database/db';

export default async () => {
  await knex.schema.dropTable('transactions').dropTable('category').dropTable('wallets');
  await knex.destroy();
}