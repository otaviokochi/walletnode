import knex from './database/db';

export default async () => {
  await knex.schema.createTable('wallets', (table) => {
    table.increments('id');
    table.string('name').notNullable();
    table.float('money').notNullable();
    table.string('cpf', 11).notNullable().unique();
    table.string('password').notNullable();
  });
  await knex.schema.createTable('category', (table) => {
    table.increments('id');
    table.integer('idUser');
    table.string('name').notNullable();

    table.unique(["idUser", "name"]);
    table.foreign('idUser').references('id').inTable('wallets');
  });

  await knex.schema.createTable('transactions', (table) => {
    table.increments('id');
    table.integer('idUserWhoSent');
    table.string('nameUserWhoSent').notNullable();
    table.float('transferredMoney').notNullable();
    table.integer('idUserWhoReceived');
    table.string('nameUserWhoReceived').notNullable();
    table.string('obs');
    table.integer('category');

    table.timestamp('created_at').defaultTo(knex.fn.now());

    table.foreign('idUserWhoSent').references('id').inTable('wallets');
    table.foreign('idUserWhoReceived').references('id').inTable('wallets');
    table.foreign('category').references('id').inTable('category');
  });

  await knex("wallets").del();

  // Inserts seed entries
  await knex("wallets").insert([
    {
      id: 1,
      name: 'otavio',
      cpf: '1111',
      money: 1050.5,
      password: "$2b$10$nGTRXACIrvX/RuiEfPcJNeC8bEsYqWSQ8IZTCSVMbRnUJZ7FgoSzi" //password = otavio
    },
    {
      id: 2,
      name: 'Usuário 1',
      cpf: '1010',
      money: 150.3,
      password: "$2b$10$nGTRXACIrvX/RuiEfPcJNeC8bEsYqWSQ8IZTCSVMbRnUJZ7FgoSzi" //password = otavio
    },
    {
      id: 3,
      name: 'Usuário 2',
      cpf: '0101',
      money: 540,
      password: "$2b$10$nGTRXACIrvX/RuiEfPcJNeC8bEsYqWSQ8IZTCSVMbRnUJZ7FgoSzi" //password = otavio
    },
    {
      id: 4,
      name: 'teste',
      cpf: '0000',
      money: 509.75,
      password: "$2b$10$xNy/XrXs0NC2GpAEzhs7z.vc.ltDTxCCLgu7cCPPW7poHbCk.pvOq" //password = teste
    },
  ]);
  await knex.raw('select setval(\'wallets_id_seq\', max(id)) from wallets');

  await knex("category").del();
  await knex("category").insert([
    {
      id: 1,
      idUser: 1,
      name: "categoria usuario 1"
    },
    {
      id: 2,
      idUser: 3,
      name: "categoria usuario 3"
    },
    {
      id: 3,
      idUser: 4,
      name: "categoria usuario 4"
    },
    {
      id: 4,
      idUser: 2,
      name: "categoria usuario 2"
    },
  ]);

  await knex.raw('select setval(\'category_id_seq\', max(id)) from category');
}
