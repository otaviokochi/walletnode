module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testTimeout: 20000,
  globalSetup: "./setup.ts",
  globalTeardown: './teardown.ts'
};