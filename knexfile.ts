import dotenv from "dotenv";
dotenv.config();

const database = process.env.NODE_ENV == 'test' ? process.env.DATABASE_TEST : process.env.DATABASE;
const user = process.env.USER_DB;
const password = process.env.PASSWORD;
// Update with your config settings.
export default {
  development: {
    client: 'postgresql',
    connection: {
      database,
      user,
      password,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: `${__dirname}/database/migrations`
    },
    seeds: {
      directory: `${__dirname}/database/seeds`
    }
  },
};
