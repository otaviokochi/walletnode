import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("wallets").del();

  // Inserts seed entries
  await knex("wallets").insert([
    {
      id: 1,
      name: 'otavio',
      cpf: '1111',
      money: 1050.5,
      password: "$2b$10$nGTRXACIrvX/RuiEfPcJNeC8bEsYqWSQ8IZTCSVMbRnUJZ7FgoSzi" //password = otavio <----- use this password to login
    },
    {
      id: 2,
      name: 'Usuário 1',
      cpf: '1010',
      money: 150.3,
      password: "$2b$10$nGTRXACIrvX/RuiEfPcJNeC8bEsYqWSQ8IZTCSVMbRnUJZ7FgoSzi" //password = otavio <----- use this password to login
    },
    {
      id: 3,
      name: 'Usuário 2',
      cpf: '0101',
      money: 540,
      password: "$2b$10$nGTRXACIrvX/RuiEfPcJNeC8bEsYqWSQ8IZTCSVMbRnUJZ7FgoSzi" //password = otavio <----- use this password to login
    },
    {
      id: 4,
      name: 'teste',
      cpf: '0000',
      money: 509.75,
      password: "$2b$10$xNy/XrXs0NC2GpAEzhs7z.vc.ltDTxCCLgu7cCPPW7poHbCk.pvOq" //password = teste <----- use this password to login
    },
  ]);
  
  await knex.raw('select setval(\'wallets_id_seq\', max(id)) from wallets');
};

