import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("category").del();

  // Inserts seed entries
  await knex("category").insert([
    {
      id: 1,
      idUser: 1,
      name: "categoria usuario 1"
    },
    {
      id: 2,
      idUser: 3,
      name: "categoria usuario 3"
    },
    {
      id: 3,
      idUser: 4,
      name: "categoria usuario 4"
    },
    {
      id: 4,
      idUser: 2,
      name: "categoria usuario 2"
    },
  ]);

  await knex.raw('select setval(\'category_id_seq\', max(id)) from category');
};

