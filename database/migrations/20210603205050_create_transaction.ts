import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('transactions', (table: Knex.TableBuilder) => {
    table.increments('id');
    table.integer('idUserWhoSent');
    table.string('nameUserWhoSent').notNullable();
    table.float('transferredMoney').notNullable();
    table.integer('idUserWhoReceived');
    table.string('nameUserWhoReceived').notNullable();
    table.string('obs');
    table.integer('category');

    table.timestamp('created_at').defaultTo(knex.fn.now());

    table.foreign('idUserWhoSent').references('id').inTable('wallets');
    table.foreign('idUserWhoReceived').references('id').inTable('wallets');
    table.foreign('category').references('id').inTable('category');
  });

}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('transactions');
}