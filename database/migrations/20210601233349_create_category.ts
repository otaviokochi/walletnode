import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('category', (table: Knex.TableBuilder) => {
    table.increments('id');
    table.integer('idUser');
    table.string('name').notNullable();

    table.unique(["idUser", "name"]);
    table.foreign('idUser').references('id').inTable('wallets');
  });
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('category');
}

