import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('wallets', (table: Knex.TableBuilder) => {
    table.increments('id');
    table.string('name').notNullable();
    table.float('money').notNullable();
    table.string('cpf', 11).notNullable().unique();
    table.string('password').notNullable();
  });
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('wallets');
}
