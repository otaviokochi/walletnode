import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('payments', (table: Knex.TableBuilder) => {
    table.increments('id');
    table.string('dueDate').notNullable();
    //1 - daily | 2 - weekly | 3 - fortnightly | 4 - semesterly | 5 - annually
    table.integer('periodicity').notNullable();
    table.integer('timesWillBeRepeated').notNullable();
    table.integer('timesExecuted').notNullable();
    table.integer('idUserWhoSent');
    table.string('nameUserWhoSent').notNullable();
    table.integer('idUserWhoReceived');
    table.string('nameUserWhoReceived').notNullable();
    table.boolean('automaticPayment').notNullable();
    table.boolean('isActive').notNullable();
    table.float('paymentMoney').notNullable();
    
    table.timestamp('created_at').defaultTo(knex.fn.now());

    table.foreign('idUserWhoSent').references('id').inTable('wallets');
    table.foreign('idUserWhoReceived').references('id').inTable('wallets');
  });

}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('payments');

}

